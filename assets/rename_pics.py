import os
import shutil


def rename_pics(dir_in, dir_out, basename = 'Image', ext = '.jpg'):

	file_counter = 1

	filepaths = (os.path.abspath(os.path.join(dir_in, p)) for p in os.listdir(dir_in) )

	for filepath in filepaths:
		
		newname = generate_file_name(basename, ext, file_counter)

		newpath = os.path.join(dir_out, newname)

		shutil.copy(filepath, newpath)

		file_counter += 1

def generate_file_name(basename, ext, num):

    s = "{0}_{1:0>5d}{2}".format(basename, num, ext)

    return s

if __name__ == "__main__":

	import sys
	rename_pics(sys.argv[1], sys.argv[2])





